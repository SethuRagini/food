package com.hcl.learn.food.search.add.service;

import java.util.List;

import com.hcl.learn.food.search.add.dto.ApiResponse;
import com.hcl.learn.food.search.add.dto.CartItemDto;
import com.hcl.learn.food.search.add.dto.UserDto;
import com.hcl.learn.food.search.add.entity.FoodItems;

public interface UserService {

	ApiResponse userRegister(UserDto userDto);

	ApiResponse addToCart(String email,long foodId, CartItemDto cartItemDto);

List<FoodItems> searchFoodItems(String foodName, String foodType);

	ApiResponse addOrders(long cartItemId,String email);

	


}
