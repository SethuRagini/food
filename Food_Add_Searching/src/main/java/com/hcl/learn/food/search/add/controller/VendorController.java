package com.hcl.learn.food.search.add.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.learn.food.search.add.dto.ApiResponse;
import com.hcl.learn.food.search.add.dto.FoodItemDto;
import com.hcl.learn.food.search.add.dto.VenderAdminDto;
import com.hcl.learn.food.search.add.service.VenderAdminService;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;

@RestController
@RequestMapping("")

public class VendorController {

	private final VenderAdminService venderAdminService;

	@Autowired
	public VendorController(VenderAdminService venderAdminService) {
		super();
		this.venderAdminService = venderAdminService;
	}

	@PostMapping("/vendor")
	public ResponseEntity<ApiResponse> addVender(@Valid @RequestBody VenderAdminDto venderAdminDto) {
		return new ResponseEntity<>(venderAdminService.create(venderAdminDto), HttpStatus.CREATED);
	}

	@PostMapping("/login")
	public ResponseEntity<ApiResponse> login(@Valid @NotBlank @RequestParam String email,
			@RequestParam String password) {
		return new ResponseEntity<>(venderAdminService.login(email, password), HttpStatus.OK);
	}

	@PostMapping("/food/add/{id}")
	public ResponseEntity<ApiResponse> addFood(@PathVariable long id, @Valid @RequestBody FoodItemDto foodItemDto) {
		return new ResponseEntity<>(venderAdminService.addFood(id, foodItemDto), HttpStatus.CREATED);
	}

	@PutMapping("/{foodItemId}")
	public ResponseEntity<ApiResponse> updateFoodItem(@RequestHeader("email") String email,
			@PathVariable("foodItemId") Long foodItemId, @RequestBody FoodItemDto foodItemDto) {
		return ResponseEntity.status(HttpStatus.OK).body(venderAdminService.updateFood(email, foodItemId, foodItemDto));
	}

	@DeleteMapping("/food/{foodItemId}")
	public ResponseEntity<ApiResponse> deleteFood(@PathVariable long foodItemId) {
		return ResponseEntity.status(HttpStatus.OK).body(venderAdminService.deleteFood(foodItemId));
	}

}
