package com.hcl.learn.food.search.add.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CartItem {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long cartItemId;
	private String vendorName;
	private String foodName;
	private int quantity;
	@ManyToOne(cascade = CascadeType.ALL)
	private UserRegistration user;
	@ManyToOne
	private FoodItems foodItems;
	private double totalPrice;
	private String paymentMethod;
	private String PaymentStatus;
}
