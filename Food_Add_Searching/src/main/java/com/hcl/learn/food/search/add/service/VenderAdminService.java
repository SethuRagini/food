package com.hcl.learn.food.search.add.service;

import com.hcl.learn.food.search.add.dto.ApiResponse;
import com.hcl.learn.food.search.add.dto.FoodItemDto;
import com.hcl.learn.food.search.add.dto.VenderAdminDto;

public interface VenderAdminService {

	ApiResponse create( VenderAdminDto venderAdminDto);

	ApiResponse login(String email, String password);

	ApiResponse addFood(long id, FoodItemDto foodItemDto);

	ApiResponse updateFood(String email, Long foodItemId, FoodItemDto foodItemDto);

	ApiResponse deleteFood(long foodItemId);

	Object deleteFood(Object object);




}
