package com.hcl.learn.food.search.add.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.learn.food.search.add.entity.CartItem;

public interface CartItemRepository extends JpaRepository<CartItem, Long> {

}
