package com.hcl.learn.food.search.add.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.learn.food.search.add.entity.Orders;

public interface OrderRepository extends JpaRepository<Orders, Long>{

}
