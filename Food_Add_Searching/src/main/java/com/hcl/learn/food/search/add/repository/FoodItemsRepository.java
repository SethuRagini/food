package com.hcl.learn.food.search.add.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.learn.food.search.add.entity.FoodItems;

public interface FoodItemsRepository extends JpaRepository<FoodItems, Long> {

	Optional<FoodItems> findByFoodNameIgnoreCase(String foodName);

	List<FoodItems> findByFoodNameContainingIgnoreCaseAndFoodTypeContainingIgnoreCase(String foodName, String foodType);



	List<FoodItems> findByFoodNameContainingIgnoreCase(String foodName);

	List<FoodItems> findByFoodTypeContainingIgnoreCase(String foodType);



}
