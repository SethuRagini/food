package com.hcl.learn.food.search.add.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.learn.food.search.add.dto.ApiResponse;
import com.hcl.learn.food.search.add.dto.CartItemDto;
import com.hcl.learn.food.search.add.dto.UserDto;
import com.hcl.learn.food.search.add.entity.FoodItems;
import com.hcl.learn.food.search.add.service.UserService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("api/")
public class UserController {

	private final UserService userService;

	@Autowired
	public UserController(UserService userService) {
		super();
		this.userService = userService;
	}

	@GetMapping
	public ResponseEntity<List<FoodItems>> searchFoodItems(@RequestParam(required = false) String foodName,
			@RequestParam(required = false) String foodType) {
		List<FoodItems> foodItems = userService.searchFoodItems(foodName, foodType);
		return new ResponseEntity<>(foodItems, HttpStatus.OK);
	}

	@PostMapping("/user")
	public ResponseEntity<ApiResponse> userRegister(@Valid @RequestBody UserDto userDto) {
		return new ResponseEntity<>(userService.userRegister(userDto), HttpStatus.CREATED);
	}

	@PostMapping("cart/{email}")
	public ResponseEntity<ApiResponse> aaddToCart(@PathVariable String email,@PathVariable long foodId,
			@Valid @RequestBody CartItemDto cartItemDto) {
		return new ResponseEntity<>(userService.addToCart(email, foodId,cartItemDto), HttpStatus.CREATED);
	}
	
	@PostMapping("/{cartid}")
	public ResponseEntity<ApiResponse> addOrder( @PathVariable long cartItemId,@RequestParam String email) {
		return ResponseEntity.status(HttpStatus.CREATED)
				.body(userService.addOrders( cartItemId,email));
	}

}
