package com.hcl.learn.food.search.add.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.learn.food.search.add.dto.ApiResponse;
import com.hcl.learn.food.search.add.dto.CartItemDto;
import com.hcl.learn.food.search.add.dto.UserDto;
import com.hcl.learn.food.search.add.entity.CartItem;
import com.hcl.learn.food.search.add.entity.FoodItems;
import com.hcl.learn.food.search.add.entity.UserRegistration;
import com.hcl.learn.food.search.add.entity.VenderDetails;
import com.hcl.learn.food.search.add.exception.ResourceConflictExists;
import com.hcl.learn.food.search.add.exception.ResourceNotFound;
import com.hcl.learn.food.search.add.exception.UnauthorizedUser;
import com.hcl.learn.food.search.add.repository.CartItemRepository;
import com.hcl.learn.food.search.add.repository.FoodItemsRepository;
import com.hcl.learn.food.search.add.repository.UserRegistrationRepository;
import com.hcl.learn.food.search.add.repository.VenderAdminRepository;
import com.hcl.learn.food.search.add.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private final UserRegistrationRepository userRegistrationRepository;
	private final FoodItemsRepository foodItemsRepository;
	private final VenderAdminRepository venderAdminRepository;
	private final CartItemRepository cartItemRepository;

	@Autowired
	public UserServiceImpl(UserRegistrationRepository userRegistrationRepository,
			FoodItemsRepository foodItemsRepository, VenderAdminRepository venderAdminRepository,
			CartItemRepository cartItemRepository) {
		super();
		this.userRegistrationRepository = userRegistrationRepository;
		this.venderAdminRepository = venderAdminRepository;
		this.foodItemsRepository = foodItemsRepository;
		this.cartItemRepository = cartItemRepository;
	}

	@Override
	public ApiResponse userRegister(UserDto userDto) {
		Optional<UserRegistration> byEmail = userRegistrationRepository.findByEmail(userDto.getUserEmail());
		if (byEmail.isPresent()) {
			throw new ResourceConflictExists("User email Id already register..");
		}

		UserRegistration user = UserRegistration.builder().email(userDto.getUserEmail()).name(userDto.getUserName())
				.password(userDto.getPassword()).phone(userDto.getUserPhone()).build();

		userRegistrationRepository.save(user);
		return ApiResponse.builder().code(201l).message("succefuuly register..").build();
	}
	

	@Override
	public ApiResponse addToCart(String email, long foodId, CartItemDto cartItemDto) {
		UserRegistration registration = userRegistrationRepository.findByEmail(email)
				.orElseThrow(() -> new ResourceConflictExists("User email id is not exits.."));
		FoodItems items = foodItemsRepository.findById(foodId)
				.orElseThrow(() -> new ResourceNotFound("Invalid Food ID"));

		if (registration.isLoggedIn()) {

			FoodItems foodName = foodItemsRepository.findByFoodNameIgnoreCase(cartItemDto.getFoodName())
					.orElseThrow(() -> new ResourceNotFound("food items not exits"));
			String food = foodName.getFoodName();

			VenderDetails vendorName = venderAdminRepository.findByNameIgnoreCase(cartItemDto.getVendorName())
					.orElseThrow(() -> new ResourceNotFound("Vender name not exits"));
			String vendeName = vendorName.getName();
			double price = foodName.getFoodPrice() * cartItemDto.getQuantity();

			CartItem cartItem = CartItem.builder().foodName(food).vendorName(vendeName).totalPrice(price)
					.quantity(cartItemDto.getQuantity()).foodItems(items).paymentMethod(cartItemDto.getPaymentMethod())
					.PaymentStatus("pending").build();

			cartItemRepository.save(cartItem);
			return ApiResponse.builder().code(201l).message("food add cart successfully..").build();
		} else
			throw new UnauthorizedUser("User must logged in to add the cart");
	}

	@Override
	public List<FoodItems> searchFoodItems(String foodName, String foodType) {

		if (foodName != null && foodType != null) {
			return foodItemsRepository.findByFoodNameContainingIgnoreCaseAndFoodTypeContainingIgnoreCase(foodName, foodType);
		} else if (foodName != null) {
			return foodItemsRepository.findByFoodNameContainingIgnoreCase(foodName);
		} else if (foodType != null) {
			return foodItemsRepository.findByFoodTypeContainingIgnoreCase(foodType);
		} else {

			return foodItemsRepository.findAll();
		}
	}

	@Override
	public ApiResponse addOrders(long cartItemId, String email) {

		UserRegistration registration = userRegistrationRepository.findByEmail(email)
				.orElseThrow(() -> new ResourceNotFound("User email id is not exits.."));
		CartItem cartItem = cartItemRepository.findById(cartItemId)
				.orElseThrow(() -> new ResourceNotFound("Invalid CartID"));
		if (registration.isLoggedIn()) {
			cartItem.setPaymentStatus("Done");
			cartItemRepository.save(cartItem);
			return ApiResponse.builder().code(200l).message("Payment Succefully done..").build();

		} else
			throw new UnauthorizedUser("Customer Should be login to Conform the payment");
	}

}
