package com.hcl.learn.food.search.add.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class FoodItemsDisplayDto {
	
	private String foodName;
	private int foodPrice;
	private String foodType;
	private String hotelName;

}
