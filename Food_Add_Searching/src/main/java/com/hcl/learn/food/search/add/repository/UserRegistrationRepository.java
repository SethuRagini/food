package com.hcl.learn.food.search.add.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.learn.food.search.add.entity.UserRegistration;


public interface UserRegistrationRepository extends JpaRepository<UserRegistration, Long> {

	Optional<UserRegistration>  findByEmail(String email);
}
