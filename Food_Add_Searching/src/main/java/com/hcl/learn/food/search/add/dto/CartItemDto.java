package com.hcl.learn.food.search.add.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CartItemDto {
	
	@NotBlank(message = "please enter vendor name")
	private String vendorName;
	@NotBlank(message = "please enter food name")
	private String foodName;
	private int quantity;
	private String paymentMethod;

}
