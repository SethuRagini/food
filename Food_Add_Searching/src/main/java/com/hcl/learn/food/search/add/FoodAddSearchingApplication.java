package com.hcl.learn.food.search.add;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodAddSearchingApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodAddSearchingApplication.class, args);
	}

}
