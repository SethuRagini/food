package com.hcl.learn.food.search.add.dto;

import java.util.function.BooleanSupplier;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ApiResponse {
	
	private long code;
	private String message;
	

}
