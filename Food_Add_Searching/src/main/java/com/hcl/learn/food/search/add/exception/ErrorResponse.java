package com.hcl.learn.food.search.add.exception;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorResponse {
	private int code;
	private String message;

}
