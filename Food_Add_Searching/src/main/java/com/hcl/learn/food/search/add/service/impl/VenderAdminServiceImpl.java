package com.hcl.learn.food.search.add.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import com.hcl.learn.food.search.add.dto.ApiResponse;
import com.hcl.learn.food.search.add.dto.FoodItemDto;
import com.hcl.learn.food.search.add.dto.VenderAdminDto;
import com.hcl.learn.food.search.add.entity.FoodItems;
import com.hcl.learn.food.search.add.entity.UserRegistration;
import com.hcl.learn.food.search.add.entity.VenderDetails;
import com.hcl.learn.food.search.add.exception.ResourceConflictExists;
import com.hcl.learn.food.search.add.exception.ResourceNotFound;
import com.hcl.learn.food.search.add.exception.UnauthorizedUser;
import com.hcl.learn.food.search.add.repository.FoodItemsRepository;
import com.hcl.learn.food.search.add.repository.UserRegistrationRepository;
import com.hcl.learn.food.search.add.repository.VenderAdminRepository;
import com.hcl.learn.food.search.add.service.VenderAdminService;

@Service

public class VenderAdminServiceImpl implements VenderAdminService {

	private final VenderAdminRepository venderAdminRepository;
	private final FoodItemsRepository foodItemsRepository;
	private final UserRegistrationRepository userRegistrationRepository;

	@Autowired
	public VenderAdminServiceImpl(VenderAdminRepository venderAdminRepository, FoodItemsRepository foodItemsRepository,
			UserRegistrationRepository userRegistrationRepository) {
		super();
		this.venderAdminRepository = venderAdminRepository;
		this.foodItemsRepository = foodItemsRepository;
		this.userRegistrationRepository = userRegistrationRepository;
	}

	@Override
	public ApiResponse create(VenderAdminDto venderAdminDto) {
		Optional<VenderDetails> admin = venderAdminRepository.findByEmail(venderAdminDto.getVenderEmail());
		if (admin.isPresent()) {
			throw new ResourceConflictExists("Vender admin already register");
		}
		VenderDetails admin1 = VenderDetails.builder().email(venderAdminDto.getVenderEmail())
				.name(venderAdminDto.getVendeName()).password(venderAdminDto.getPassword())
				.phone(venderAdminDto.getVenderPhone()).build();
		venderAdminRepository.save(admin1);

		return ApiResponse.builder().code(201l).message("Successfully Vender Register").build();
	}

	@Override
	public ApiResponse login(String email, String password) {
 
		Optional<VenderDetails> byEmail = venderAdminRepository.findByEmail(email);
		if (byEmail.isPresent()) {
			Optional<VenderDetails> byEmail2 = venderAdminRepository.findByEmail(email);
 
			VenderDetails details = byEmail2.get();
			if (details.getPassword().equals(password)) {
				details.setLoggedIn(true);
				venderAdminRepository.save(details);
				return ApiResponse.builder().code(200l).message("Vendor login Successfully").build();
			} else
				throw new ResourceConflictExists("Please enter correct password");
		} else {
			UserRegistration userRegistration = userRegistrationRepository.findByEmail(email)
					.orElseThrow(() -> new UnauthorizedUser("User not exits"));
			if (userRegistration.getPassword().equals(password)) {
				userRegistration.setLoggedIn(true);
				userRegistrationRepository.save(userRegistration);
				return ApiResponse.builder().code(200l).message("User login successfully").build();
			} else
				throw new ResourceConflictExists("Please enter correct password");
		}
 
	}

	@Override
	public ApiResponse addFood(long id, FoodItemDto foodItemDto) {
		VenderDetails adminRegister = venderAdminRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFound("Vender email Id not exits"));

		if (adminRegister.isLoggedIn()) {
			Optional<FoodItems> byFoodNameIgnoreCase = foodItemsRepository
					.findByFoodNameIgnoreCase(foodItemDto.getFoodName());
			if (byFoodNameIgnoreCase.isEmpty()) {

				FoodItems fItems = FoodItems.builder().foodName(foodItemDto.getFoodName())
						.foodPrice(foodItemDto.getPrice()).foodItemsAddBy(foodItemDto.getFoodItemsAddBy())
						.vendor(adminRegister).build();

				foodItemsRepository.save(fItems);
				return ApiResponse.builder().code(201l).message("Food items add successfully").build();
			} else
				throw new ResourceConflictExists("Food already present");
		} else
			throw new UnauthorizedUser("Vender Login to to add the food");

	}

	@Override
	public ApiResponse updateFood(String email, Long foodItemId, FoodItemDto foodItemDto) {
		Optional<VenderDetails> byEmail = venderAdminRepository.findByEmail(email);
		if (byEmail.isEmpty()) {
			throw new ResourceNotFound("Vender email Id does not exist");
		}
		VenderDetails admin = byEmail.get();
		if (admin.isLoggedIn()) {
			Optional<FoodItems> optionalFoodItem = foodItemsRepository.findById(foodItemId);
			if (optionalFoodItem.isPresent()) {
				FoodItems foodItem = optionalFoodItem.get();
				foodItem.setFoodName(foodItemDto.getFoodName());
				foodItem.setFoodPrice(foodItemDto.getPrice());
				foodItemsRepository.save(foodItem);
				return ApiResponse.builder().code(200l).message("Food item updated successfully").build();
			} else {
				throw new ResourceNotFound("Food item not found");
			}
		} else {
			throw new UnauthorizedUser("Vender must be logged in to update food items");
		}
	}

	@Override
	public ApiResponse deleteFood(long foodItemId) {
		FoodItems foodItems = foodItemsRepository.findById(foodItemId)
				.orElseThrow(() -> new ResourceNotFound("Foood not found"));
		VenderDetails vendor = foodItems.getVendor();
		if (vendor == null || !vendor.isLoggedIn()) {
			throw new ResourceConflictExists("Vendor should be logged in");
		}
		foodItemsRepository.deleteById(foodItemId);
		return ApiResponse.builder().code(200l).message("Food deleted successfully").build();
	}

	@Override
	public Object deleteFood(Object object) {
		// TODO Auto-generated method stub
		return null;
	}

}
