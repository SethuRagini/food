package com.hcl.learn.food.search.add.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.learn.food.search.add.entity.VenderDetails;



public interface VenderAdminRepository extends JpaRepository<VenderDetails, Long> {
	
	Optional<VenderDetails> findByEmail(String email);
	
	Optional<VenderDetails> findByNameIgnoreCase(String name);

}
