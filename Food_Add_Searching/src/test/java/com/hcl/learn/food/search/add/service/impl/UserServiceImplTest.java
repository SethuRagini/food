package com.hcl.learn.food.search.add.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.hcl.learn.food.search.add.dto.ApiResponse;
import com.hcl.learn.food.search.add.dto.CartItemDto;
import com.hcl.learn.food.search.add.entity.CartItem;
import com.hcl.learn.food.search.add.entity.FoodItems;
import com.hcl.learn.food.search.add.entity.UserRegistration;
import com.hcl.learn.food.search.add.entity.VenderDetails;
import com.hcl.learn.food.search.add.exception.ResourceConflictExists;
import com.hcl.learn.food.search.add.exception.ResourceNotFound;
import com.hcl.learn.food.search.add.exception.UnauthorizedUser;
import com.hcl.learn.food.search.add.repository.CartItemRepository;
import com.hcl.learn.food.search.add.repository.FoodItemsRepository;
import com.hcl.learn.food.search.add.repository.UserRegistrationRepository;
import com.hcl.learn.food.search.add.repository.VenderAdminRepository;
	 
	public class UserServiceImplTest {
	 
	    @Mock
	    private UserRegistrationRepository userRegistrationRepository;
	 
	    @Mock
	    private FoodItemsRepository foodItemsRepository;
	 
	    @Mock
	    private VenderAdminRepository venderAdminRepository;
	 
	    @Mock
	    private CartItemRepository cartItemRepository;
	 
	    @InjectMocks
	    private UserServiceImpl userService;
	 
	    @BeforeEach
	    public void setUp() {
	        MockitoAnnotations.initMocks(this);
	    }
	 
	
	 
	    @Test
	    public void testAddToCart_UnauthorizedUser() {
	        // Mocking
	        UserRegistration registration = new UserRegistration();
	        registration.setLoggedIn(false);
	        when(userRegistrationRepository.findByEmail(anyString())).thenReturn(Optional.of(registration));
	        
	        FoodItems food = new FoodItems();
	        food.setFoodId(1l);
	        when(foodItemsRepository.findById(1l)).thenReturn(Optional.of(food));
	        // Test
	        try {
	            userService.addToCart("test@example.com", 1L, new CartItemDto());
	        } catch (UnauthorizedUser e) {
	            // Verify
	            assertEquals("User must logged in to add the cart", e.getMessage());
	        }
	    }
	    @Test
	    public void testAddToCart_Negative_ResourceNotFound() {
	    	
	    	String email = "ragini@email.com";
	        // Mocking
	        when(userRegistrationRepository.findByEmail(email)).thenReturn(java.util.Optional.empty());
	 
	        // Test
	        try {
	            userService.addToCart(email, 1L, new CartItemDto());
	        } catch (ResourceConflictExists e) {
	            // Verify
	            assertEquals("User email id is not exits..", e.getMessage());
	        }
	    }
	 
	    @Test
	    public void testAddToCart_Negative_UnauthorizedUser() {
	        // Mocking
	        UserRegistration registration = new UserRegistration();
	        registration.setLoggedIn(false);
	        when(userRegistrationRepository.findByEmail(anyString())).thenReturn(Optional.of(registration));
	        
	        FoodItems food= new FoodItems();
	        food.setFoodId(1l);
	        food.setFoodName("Idli");
	        when(foodItemsRepository.findById(1l)).thenReturn(Optional.of(food));
	        VenderDetails venderDetails = new VenderDetails();
	        venderDetails.setName("Udapi");
	        
	        when(venderAdminRepository.findByNameIgnoreCase("Udapi")).thenReturn(Optional.of(venderDetails));
	        
	        CartItemDto cart = new CartItemDto();
	        cart.setFoodName("Dosa");
	        cart.setVendorName("Udapi");
	        cart.setQuantity(3);
	        
	        // Test
	        try {
	            userService.addToCart("test@example.com", 1L, cart);
	        } catch (UnauthorizedUser e) {
	            // Verify
	            assertEquals("User must logged in to add the cart", e.getMessage());
	        }
	    }
	 
	    @Test
	    public void testSearchFoodItems_Positive() {
	        // Mocking
	        List<FoodItems> foodItemsList = Arrays.asList(new FoodItems(), new FoodItems());
	        when(foodItemsRepository.findAll()).thenReturn(foodItemsList);
	 
	        // Test
	        List<FoodItems> result = userService.searchFoodItems(null, null);
	 
	        // Verify
	        assertEquals(2, result.size());
	    }
	 
	    @Test
	    public void testSearchFoodItems_ByFoodName() {
	        // Mocking
	        List<FoodItems> foodItemsList = Arrays.asList(new FoodItems(), new FoodItems());
	        when(foodItemsRepository.findByFoodNameContainingIgnoreCase(anyString())).thenReturn(foodItemsList);
	 
	        // Test
	        List<FoodItems> result = userService.searchFoodItems("Pizza", null);
	 
	        // Verify
	        assertEquals(2, result.size());
	    }
	 
	    @Test
	    public void testSearchFoodItems_ByFoodType() {
	        // Mocking
	        List<FoodItems> foodItemsList = Arrays.asList(new FoodItems(), new FoodItems());
	        when(foodItemsRepository.findByFoodTypeContainingIgnoreCase(anyString())).thenReturn(foodItemsList);
	 
	        // Test
	        List<FoodItems> result = userService.searchFoodItems(null, "Italian");
	 
	        // Verify
	        assertEquals(2, result.size());
	    }
	 
	    @Test
	    public void testSearchFoodItems_ByFoodNameAndType() {
	        // Mocking
	        List<FoodItems> foodItemsList = Arrays.asList(new FoodItems(), new FoodItems());
	        when(foodItemsRepository.findByFoodNameContainingIgnoreCaseAndFoodTypeContainingIgnoreCase(anyString(), anyString())).thenReturn(foodItemsList);
	 
	        // Test
	        List<FoodItems> result = userService.searchFoodItems("Pizza", "Italian");
	 
	        // Verify
	        assertEquals(2, result.size());
	    }
	 
	    @Test
	    public void testAddOrders_Positive() {
	        // Mocking
	        UserRegistration registration = new UserRegistration();
	        registration.setLoggedIn(true);
	        when(userRegistrationRepository.findByEmail(anyString())).thenReturn(Optional.of(registration));
	 
	        CartItem cartItem = new CartItem();
	        when(cartItemRepository.findById(anyLong())).thenReturn(Optional.of(cartItem));
	 
	        // Test
	        ApiResponse response = userService.addOrders(1L, "test@example.com");
	 
	        // Verify
	        assertEquals(200L, response.getCode());
	        assertEquals("Payment Succefully done..", response.getMessage());
	        assertEquals("Done", cartItem.getPaymentStatus());
	    }
	 
	    @Test
	    public void testAddOrders_UnauthorizedUser() {
	        // Mocking
	        UserRegistration registration = new UserRegistration();
	        registration.setEmail("Praveen@mail.com");
	        registration.setLoggedIn(false);
	        when(userRegistrationRepository.findByEmail("Praveen@mail.com")).thenReturn(Optional.of(registration));
	        
	        CartItem cart = new CartItem();
	        cart.setCartItemId(1l);
	        when(cartItemRepository.findById(1l)).thenReturn(Optional.of(cart));
	        // Test
	        try {
	            userService.addOrders(1L, "Praveen@mail.com");
	        } catch (UnauthorizedUser e) {
	            // Verify
	            assertEquals("Customer Should be login to Conform the payment", e.getMessage());
	        }
	    }
	}

