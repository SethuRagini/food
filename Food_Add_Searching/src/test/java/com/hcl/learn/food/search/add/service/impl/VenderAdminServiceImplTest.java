package com.hcl.learn.food.search.add.service.impl;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.hcl.learn.food.search.add.dto.ApiResponse;
import com.hcl.learn.food.search.add.dto.FoodItemDto;
import com.hcl.learn.food.search.add.dto.VenderAdminDto;
import com.hcl.learn.food.search.add.entity.FoodItems;
import com.hcl.learn.food.search.add.entity.UserRegistration;
import com.hcl.learn.food.search.add.entity.VenderDetails;
import com.hcl.learn.food.search.add.exception.ResourceConflictExists;
import com.hcl.learn.food.search.add.exception.ResourceNotFound;
import com.hcl.learn.food.search.add.exception.UnauthorizedUser;
import com.hcl.learn.food.search.add.repository.FoodItemsRepository;
import com.hcl.learn.food.search.add.repository.UserRegistrationRepository;
import com.hcl.learn.food.search.add.repository.VenderAdminRepository;
 
class VenderAdminServiceImplTest {
 
    @Mock
    private VenderAdminRepository venderAdminRepository;
 
    @Mock
    private FoodItemsRepository foodItemsRepository;
 
    @Mock
    private UserRegistrationRepository userRegistrationRepository;
 
    @InjectMocks
    private VenderAdminServiceImpl venderAdminService;
 
    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }
 
    @Test
     void testCreateVenderAdmin_Success() {
        VenderAdminDto venderAdminDto = new VenderAdminDto();
        venderAdminDto.setVenderEmail("admin@example.com");
        venderAdminDto.setVendeName("Admin");
        venderAdminDto.setPassword("password");
        venderAdminDto.setVenderPhone("1234567890");
 
        when(venderAdminRepository.findByEmail("admin@example.com")).thenReturn(Optional.empty());
 
        ApiResponse response = venderAdminService.create(venderAdminDto);
 
        assertNotNull(response);
        assertEquals(201L, response.getCode());
        assertEquals("Successfully Vender Register", response.getMessage());
    }
 
    @Test
     void testCreateVenderAdmin_Failure_EmailAlreadyExists() {
        VenderAdminDto venderAdminDto = new VenderAdminDto();
        venderAdminDto.setVenderEmail("admin@example.com");
        venderAdminDto.setVendeName("Admin");
        venderAdminDto.setPassword("password");
        venderAdminDto.setVenderPhone("1234567890");
 
        when(venderAdminRepository.findByEmail("admin@example.com"))
                .thenReturn(Optional.of(new VenderDetails()));
 
        assertThrows(ResourceConflictExists.class, () -> venderAdminService.create(venderAdminDto));
    }
//    @Test
//    public void testUserLoginSuccess() {
//        // Prepare test data
//        UserRegistration userRegistration = new UserRegistration();
//        when(venderAdminRepository.findByEmail("email")).thenReturn(Optional.empty());
//        when(userRegistrationRepository.findByEmail("email")).thenReturn(Optional.of(userRegistration));
// 
//        // Call the method under test
//        ApiResponse response = VenderAdminServiceImplTest .login("email", "password");
// 
//        // Verify the response
//        assertEquals(200L, response.getCode());
//        assertTrue(response.getMessage().contains("User login successfully"));
//        assertTrue(userRegistration.isLoggedIn());
//        verify(userRegistrationRepository, times(1)).save(userRegistration);
//    }
 
    

	@Test
    public void testInvalidPassword() {
        // Prepare test data
        VenderDetails vendorDetails = new VenderDetails();
        when(venderAdminRepository.findByEmail("email")).thenReturn(Optional.of(vendorDetails));
 
        // Call the method under test with an incorrect password
        VenderAdminServiceImplTest .login("email", "wrongPassword");
    }
 
    

	private static void login(String string, String string2) {
		// TODO Auto-generated method stub
		
	}

	@Test
    public void testUserNotFound() {
        // Prepare test data
        when(venderAdminRepository.findByEmail("nonexistentEmail")).thenReturn(Optional.empty());
        when(userRegistrationRepository.findByEmail("nonexistentEmail")).thenReturn(Optional.empty());
 
        // Call the method under test with a non-existent email
        VenderAdminServiceImplTest .login("nonexistentEmail", "password");
    }



    @Test
    public void testAddFood_Success() {
        VenderDetails venderDetails = new VenderDetails();
        venderDetails.setId(1L);
        venderDetails.setLoggedIn(true);
 
        FoodItemDto foodItemDto = new FoodItemDto();
        foodItemDto.setFoodName("Pizza");
        foodItemDto.setPrice(10.0);
        foodItemDto.setFoodItemsAddBy("Admin");
 
        when(venderAdminRepository.findById(1L)).thenReturn(Optional.of(venderDetails));
        when(foodItemsRepository.findByFoodNameIgnoreCase("Pizza")).thenReturn(Optional.empty());
 
        ApiResponse response = venderAdminService.addFood(1L, foodItemDto);
 
        assertNotNull(response);
        assertEquals(201L, response.getCode());
        assertEquals("Food items add successfully", response.getMessage());
    }
 
    @Test
    public void testAddFood_Failure_VendorNotLoggedIn() {
        VenderDetails venderDetails = new VenderDetails();
        venderDetails.setId(1L);
        venderDetails.setLoggedIn(false);
 
        FoodItemDto foodItemDto = new FoodItemDto();
        foodItemDto.setFoodName("Pizza");
        foodItemDto.setPrice(10.0);
        foodItemDto.setFoodItemsAddBy("Admin");
 
        when(venderAdminRepository.findById(1L)).thenReturn(Optional.of(venderDetails));
 
        assertThrows(UnauthorizedUser.class, () -> venderAdminService.addFood(1L, foodItemDto));
    }
    @Test
     void testUpdateFood_Success() {
        VenderDetails venderDetails = new VenderDetails();
        venderDetails.setLoggedIn(true);
 
        FoodItemDto foodItemDto = new FoodItemDto();
        foodItemDto.setFoodName("Pizza");
        foodItemDto.setPrice(15.0);
 
        FoodItems foodItem = new FoodItems();
        foodItem.setFoodId(1L);
 
        when(venderAdminRepository.findByEmail(any())).thenReturn(Optional.of(venderDetails));
        when(foodItemsRepository.findById(1L)).thenReturn(Optional.of(foodItem));
 
        ApiResponse response = venderAdminService.updateFood("admin@example.com", 1L, foodItemDto);
 
        assertNotNull(response);
        assertEquals(200L, response.getCode());
        assertEquals("Food item updated successfully", response.getMessage());
    }
 
    @Test
     void testUpdateFood_Failure_UnauthorizedUser() {
        VenderDetails venderDetails = new VenderDetails();
        venderDetails.setLoggedIn(false);
 
        when(venderAdminRepository.findByEmail(any())).thenReturn(Optional.of(venderDetails));
 
        assertThrows(UnauthorizedUser.class, () -> venderAdminService.updateFood("admin@example.com", 1L, new FoodItemDto()));
    }
 
    // Similar test cases for updateFood failure scenarios...
 
//    @Test
//    void testDeleteFood_Success() {
//        VenderDetails venderDetails = new VenderDetails();
//        venderDetails.setLoggedIn(true);
// 
//        FoodItems foodItem = new FoodItems();
//        foodItem.setFoodId(1L);
// 
//        when(foodItemsRepository.findById(1L)).thenReturn(Optional.of(foodItem));
// 
//        ApiResponse response = venderAdminService.deleteFood(1L);
// 
//        assertNotNull(response);
//        assertEquals(200L, response.getCode());
//        assertEquals("Food deleted successfully", response.getMessage());
//    }
 
    @Test
     void testDeleteFood_Failure_NotFound() {
        when(foodItemsRepository.findById(1L)).thenReturn(Optional.empty());
 
        assertThrows(ResourceNotFound.class, () -> venderAdminService.deleteFood(1L));
    }
 
    // Similar test cases for deleteFood failure scenarios...
}
    // Additional positive and negative test cases for other methods...
