package com.hcl.learn.food.search.add.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.learn.food.search.add.dto.ApiResponse;
import com.hcl.learn.food.search.add.dto.CartItemDto;
import com.hcl.learn.food.search.add.dto.UserDto;
import com.hcl.learn.food.search.add.entity.FoodItems;
import com.hcl.learn.food.search.add.service.UserService;
 
public class UserControllerTest {
 
    @Test
  void testSearchFoodItems() {
        // Mocking dependencies
        UserService userService = mock(UserService.class);
        UserController userController = new UserController(userService);
 
        // Mocking behavior
        List<FoodItems> foodItems = new ArrayList<>();
        when(userService.searchFoodItems("pizza", "Italian")).thenReturn(foodItems);
 
        // Testing the controller method
        ResponseEntity<List<FoodItems>> response = userController.searchFoodItems("pizza", "Italian");
 
        // Assertions
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(foodItems, response.getBody());
    }
 
    @Test
    void testUserRegister() {
        // Mocking dependencies
        UserService userService = mock(UserService.class);
        UserController userController = new UserController(userService);
 
        // Mocking behavior
        UserDto userDto = new UserDto();
        when(userService.userRegister(userDto)).thenReturn(new ApiResponse());
 
        // Testing the controller method
        ResponseEntity<ApiResponse> response = userController.userRegister(userDto);
 
        // Assertions
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(new ApiResponse(), response.getBody());
    }
 
    @Test
     void testAddToCart() {
        // Mocking dependencies
        UserService userService = mock(UserService.class);
        UserController userController = new UserController(userService);
 
        // Mocking behavior
        CartItemDto cartItemDto = new CartItemDto();
        when(userService.addToCart("test@example.com", 1234L, cartItemDto)).thenReturn(new ApiResponse());
 
        // Testing the controller method
        ResponseEntity<ApiResponse> response = userController.aaddToCart("test@example.com", 1234L, cartItemDto);
 
        // Assertions
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(new ApiResponse(), response.getBody());
    }
 
    @Test
    void testAddOrder() {
        // Mocking dependencies
        UserService userService = mock(UserService.class);
        UserController userController = new UserController(userService);
 
        // Mocking behavior
        when(userService.addOrders(5678L, "test@example.com")).thenReturn(new ApiResponse());
 
        // Testing the controller method
        ResponseEntity<ApiResponse> response = userController.addOrder(5678L, "test@example.com");
 
        // Assertions
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals(new ApiResponse(), response.getBody());
    }
    @Test
    void testSearchFoodItems_NoParams() {
        // Mocking dependencies
        UserService userService = mock(UserService.class);
        UserController userController = new UserController(userService);
 
        // Mocking behavior
        List<FoodItems> foodItems = new ArrayList<>();
        when(userService.searchFoodItems(null, null)).thenReturn(foodItems);
 
        // Testing the controller method without parameters
        ResponseEntity<List<FoodItems>> response = userController.searchFoodItems(null, null);
 
        // Assertions
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(foodItems, response.getBody());
    }
 
    @Test
    void testUserRegister_ValidationFailure() {
        // Mocking dependencies
        UserService userService = mock(UserService.class);
        UserController userController = new UserController(userService);
 
        // Mocking behavior
        UserDto userDto = new UserDto();
        userDto.setUserName(""); // Invalid username
        when(userService.userRegister(userDto)).thenReturn(new ApiResponse(HttpStatus.BAD_REQUEST.value(), "Validation failed"));
 
        // Testing the controller method with invalid input
        ResponseEntity<ApiResponse> response = userController.userRegister(userDto);
 
        // Assertions
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals("Validation failed", response.getBody().getMessage());
    }
 
//    @Test
//    void testAddToCart_UserNotFound() {
//        // Mocking dependencies
//        UserService userService = mock(UserService.class);
//        UserController userController = new UserController(userService);
// 
//        // Mocking behavior
//        CartItemDto cartItemDto = new CartItemDto();
//        when(userService.addToCart("nonexistent@example.com", 1234L, cartItemDto))
//            .thenThrow(new UserNotFoundException("User not found"));
// 
//        // Testing the controller method when user is not found
//        ResponseEntity<ApiResponse> response = userController.aaddToCart("nonexistent@example.com", 1234L, cartItemDto);
// 
//        // Assertions
//        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
//        assertEquals("User not found", response.getBody().getMessage());
//    }
// 
//    @Test
//     void testAddOrder_CartItemNotFound() {
//        // Mocking dependencies
//        UserService userService = mock(UserService.class);
//        UserController userController = new UserController(userService);
// 
//        // Mocking behavior
//        when(userService.addOrders(9999L, "test@example.com"))
//            .thenThrow(new CartItemNotFoundException("Cart item not found"));
// 
//        // Testing the controller method when cart item is not found
//        ResponseEntity<ApiResponse> response = userController.addOrder(9999L, "test@example.com");
// 
//        // Assertions
//        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
//        assertEquals("Cart item not found", response.getBody().getMessage());
//    }
    @Test
     void testSearchFoodItems_NoResults() {
        // Mocking dependencies
        UserService userService = mock(UserService.class);
        UserController userController = new UserController(userService);
 
        // Mocking behavior
        when(userService.searchFoodItems("nonexistent", null)).thenReturn(new ArrayList<>());
 
        // Testing the controller method when no results are found
        ResponseEntity<List<FoodItems>> response = userController.searchFoodItems("nonexistent", null);
 
        // Assertions
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertTrue(response.getBody().isEmpty());
    }
 
    @Test
   void testUserRegister_Success() {
        // Mocking dependencies
        UserService userService = mock(UserService.class);
        UserController userController = new UserController(userService);
 
        // Mocking behavior
        UserDto userDto = new UserDto();
        when(userService.userRegister(userDto)).thenReturn(new ApiResponse(HttpStatus.CREATED.value(), "User registered successfully"));
 
        // Testing the controller method for successful user registration
        ResponseEntity<ApiResponse> response = userController.userRegister(userDto);
 
        // Assertions
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals("User registered successfully", response.getBody().getMessage());
    }
 
    @Test
    public void testAddToCart_Success() {
        // Mocking dependencies
        UserService userService = mock(UserService.class);
        UserController userController = new UserController(userService);
 
        // Mocking behavior
        CartItemDto cartItemDto = new CartItemDto();
        when(userService.addToCart("test@example.com", 1234L, cartItemDto)).thenReturn(new ApiResponse(HttpStatus.CREATED.value(), "Item added to cart successfully"));
 
        // Testing the controller method for successful addition to cart
        ResponseEntity<ApiResponse> response = userController.aaddToCart("test@example.com", 1234L, cartItemDto);
 
        // Assertions
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals("Item added to cart successfully", response.getBody().getMessage());
    }
 
    @Test
    public void testAddOrder_Success() {
        // Mocking dependencies
        UserService userService = mock(UserService.class);
        UserController userController = new UserController(userService);
 
        // Mocking behavior
        when(userService.addOrders(5678L, "test@example.com"))
            .thenReturn(new ApiResponse(HttpStatus.CREATED.value(), "Order placed successfully"));
 
        // Testing the controller method for successful order placement
        ResponseEntity<ApiResponse> response = userController.addOrder(5678L, "test@example.com");
 
        // Assertions
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertEquals("Order placed successfully", response.getBody().getMessage());
    }
}



